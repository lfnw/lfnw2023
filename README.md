![Build Status](https://gitlab.com/lfnw/lfnw2023/badges/main/pipeline.svg)

---

# LFNW Event Site

The LFNW Event site represents a unique LFNW event. The site is built in [lektor](https://www.getlektor.com/) and hosted through [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).

## Developing

1. Install lektor on your development environment: https://www.getlektor.com/docs/installation/
2. Pull and branch or fork [this repository](https://gitlab.com/lfnw/lfnw2023).
3. Use `lektor server` to make content changes on the site or to test code changes locally.
4. Push your code to Gitlab and create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) to this repository. Be sure to explain _why_ you are changing, not just _what_ you are changing. Make an effort to keep changes small and granular.
5. When the changes are merged to the _main_ branch, the site will be automatically rebuilt and published via Gitlab CI.

### Using Docker
Optionally, you can also bring up a local server in Docker without installing multiple libraries and other artifacts on your system by one of the following methods.  Both require that Docker is already installed and configured for your operating system.

__The first method__ is the easiest and uses the `docker-compose.yml` file in this repository.  Simply open a command prompt, chane to the directory where you have cloned this repo and execute the following command:

    docker compose up -d

The server is now running in the background on `localhost` port `5000`.  You can now go to [http://localhost:5000](http://localhost:5000). Do not forget to shutdown the server when you are done with it like so:

    docker compose down

__The second method__ is to directly run the container with changable parameters:

    docker run --rm  -v $(pwd):/opt/lektor -p 5000:5000 softinstigate/lektor server --host 0.0.0.0

Use `ctrl-c` to stop the server.  No cleanup/shutdown is needed with this method.  You can also use this method to more easily execute other Lektor commands (ie. use `--help` instead of `server --host 0.0.0.0`)

Either method works just as well for basc viewing and testing.

## Design

The site is predominantly one single monolithic page, correlating to the _splashpage_ concept in [osem](https://osem.io/#splashModal).

### Databags

Lektor uses the concept of [_data bags_](https://www.getlektor.com/docs/content/databags/) to store structured information that can be accessed from _any_ template. The following data bags are implemented:

* Basic details of the event, including the venue, are stored in the [event data bag](databags/event.ini).
* Social network links are stored in the [social data bag](databags/social.ini).
* Top-level navigation links are stored in the [navbar data bag](databags/navbar.ini).

### Content

There is one primary page on the site, implemented as the [splashpage model](models/splashpage.ini). It has a variety of fields representing the various contents of the site, and are best edited via the [lektor server](https://www.getlektor.com/docs/quickstart/#running-your-project). The page is broken into sections, and _optional_ sections have a checkmark field for whether they are included or not.

The first section, the _about_ section, includes an optional collection of calls to action, implemented as links or buttons. These actions can be added or removed via the admin interface.

The _calls_ section is for calls for sessions, sponsors, volunteers, etc. It hosts a collection of up to three calls. This section is optional.

The _program_ section includes a raw HTML field for embedding a speaker wall (for example, from [sessionize.com](https://sessionize.com/playbook/embedding#speaker-wall-7)), and a link to a complete schedule. Managing the schedule content is outside the scope of this static site. This section is optional.

The _venue map_ section renders a dynamic, navigable map of the venue. Venue data is in the [event data bag](databags/event.ini). This section is optional.

The _sponsors_ section includes a collection of Sponsor levels, which define the groups of sponsors (e.g. Title, Gold, Silver, Partners, etc.). Each Sponsor level also includes a collection of Sponsors, including a _markdown_ block that is rendered for each sponsor. The sponsor logos must be attached to the _splashpage_ (Use the _Attachments_ function of the admin interface).

Finally, social media links and an informational footer are rendered at the bottom.

### License

As the contents of this project are as much a matter of design as they are code, this project is licensed under the [Creative Commons](https://creativecommons.org/) Attribution-ShareAlike 4.0 International ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)).

Logos of sponsors are explicitly excluded from this license, and are used solely at the discretion of their respective owners. Please do not reuse or redistribute sponsor logos without the explicit permission of the respective owners.
