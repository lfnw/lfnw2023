_model: page
---
title: Code of Conduct
---
body:

LinuxFest Northwest (LFNW) brings together an open technology community. While that informal community cooperates year round through various media and projects, LFNW itself is an organized opportunity to interact directly, personally, intensively, and freely. All types of people attend the Fest to learn, teach, share and contribute to each other. The LinuxFest Northwest organizers and volunteers are committed to having a low stress, fun environment that supports the best that open source technology can offer. Fundamentally, the Fest works best when people behave well with each other. With the number of people who attend the Fest, there may be occasions when things don’t go smoothly. To assist with resolving issues that may arise during the Fest, the LFNW organizers have developed some guiding principles. These are based on the best elements of the Fest culture, along with those from other successful open source projects.

**Be courteous, respectful and considerate** — a difference of opinion is not a valid reason for personal insults or impolite behavior. Cooperation works best when people feel personally safe and comfortable. Everyone is welcome at LinuxFest Northwest, regardless of gender, sexual orientation, race, religion, disabilities, physical appearance, or any other differences. Harassment for any reason is not acceptable. The Fest is a family event, including children. So sexual references and profanity are not appropriate. People’s actions should bring credit to themselves, LinuxFest Northwest and the larger open source community. Service animals should not be petted, distracted, or interacted with in any way; they function as a tool for the accommodation of an individual's disability, and as such should be allowed to perform their work without interference.

**Be patient and generous** — support others and ask for support. If someone asks for help, it is usually because they think they need it. In some cases, it can be helpful to suggest ways for the requester to help themselves. For example, learning how to ask good questions can sometimes be a big help. Intentionally vague or hostile responses (such as “Read The Manual”) are counterproductive.

**Assume that people mean well** — start with an assumption of good intentions. Often the best answer is somewhere between competing positions. Polite disagreement can go a long way towards resolving an issue. Clarifying possible misinterpretations builds mutual respect. Take responsibility for your actions.

**Reach out for help** — In some cases, talking directly with others involved can resolve a situation. If that isn’t possible or comfortable, our organizers are available to help. LFNW volunteers can be found wearing red volunteer shirts and can assist you with locating an organizer.

**Privacy is important** — LinuxFest Northwest has always taken pride in respecting the privacy of attendees. Registration is optional to attend the Fest itself, and we do not sell or give your information away, not even to our sponsors, without your explicit permission. We also expect attendees will keep with this commitment to privacy. Ask before taking photos of people. Do not publicly post information about other attendees (names, employer, etc) without their permission. If you witness or have reason to believe the code of conduct is being violated, please bring up the topic with staff.

The organizers of LinuxFest Northwest are honored to have the opportunity of being hosts. We encourage all attendees to share in making the Fest valuable and memorable for everyone. Thank you for your thoughtfulness.

